//////////////
//// CSE 02 WelcomeClass
///
public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints WelcomeClass to teminal window
    System.out.println("  ----------- | WELCOME |-----------"); 
    System.out.println("^  ^  ^  ^  ^  ^ ");
    System.out.println("/ \/ \/ \/ \/ \/ \ ");  
    System.out.println("<-h--y--n--2--2--2->");
    System.out.println("\ /\ /\ /\ /\ /\ / ");
    System.out.println("v  v  v  v  v  v ");
    
  }
  
}