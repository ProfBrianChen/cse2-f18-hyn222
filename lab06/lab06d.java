//HyungJun Nam
//10/12/2018

import java.util.Scanner; 

public class lab06d {
 public static void main (String []args)
 {
  int numRow = 0;
  int number = 1;
  Scanner patternD = new Scanner(System.in);
  
  System.out.println("Enter an integer");
  while (true)
  {
   if (patternD.hasNextInt() == true)
   {
    number = patternD.nextInt();
    
    if(number > 0 && number < 11)
    {
     break;
    }
   }
   System.out.println("Wrong integer!");
   System.out.println("Integer should be 1~10");
   patternD.nextLine();
  }
  
  for (numRow = 1; numRow <= number; number--) //user input(num) decreases
  
  {
   for (int num = number; num >= 1; num--)// input decrease by 1

   {
    System.out.print(num + " ");
   }
   System.out.println("");
  }
 }
}