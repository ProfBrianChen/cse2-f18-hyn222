//Hyung Jun Nam
//10/12/2018

import java.util.Scanner;
 public class lab06 {
 public static void main (String []args)
 {
  Scanner patternA = new Scanner(System.in);
  int number = 0; //assign variable
  int numRows = 1;
  
  System.out.println("Enter an integer "); //input
  while (true)
  {
   if (patternA.hasNextInt() == true) //if statement
   {
    number = patternA.nextInt(); //user input
    
    if(number > 0 && number < 11) //checks the number is true
    {
     break;
    }
   }
   System.out.println("Wrong integer!");
   System.out.println("Enteger should be 1~10");
   patternA.nextLine();
  }
  
  for (numRows = 1; numRows <= number; numRows++) //num increase
  {
   for(int num = 1; num <= numRows; num++) //num increases by 1
   {
    System.out.print(num + " ");
   }
   
   System.out.println("");
  }
 }
}