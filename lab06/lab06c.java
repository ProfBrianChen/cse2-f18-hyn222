//HyungJun Nam
//10/12/2018

import java.util.Scanner;

public class lab06c {
 public static void main (String []args)
 {
  int numRow = 0;
  int number = 1;
  Scanner patternC = new Scanner(System.in);
  
  System.out.println("Enter an integer ");//assign integer
  while (true)
  {
   if (patternC.hasNextInt() == true)
   {
    number = patternC.nextInt();
    
    if(number > 0 && number < 11)
    {
     break;
    }
   }
   System.out.println("Wrong integer");
   System.out.println("Enteger should be 1~10");
   patternC.nextLine();
  }
  
  for (numRow = 1; numRow <= number; numRow++) //user input num
  {
   for(int num =1; num <= number - numRow; num++) // add rows
   {
    System.out.print(" ");
   }
  
   for (int num = numRow; num >= 1; num--) // if num is greater or equal to 1, print
   {
    System.out.print(num);
   }
   System.out.println("");
  }
 }
}