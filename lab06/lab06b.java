//HyungJun Nam
//10/12/2018

import java.util.Scanner; //imports scanner

public class lab06b {
 public static void main (String []args)
 {
  int number = 0;
  int numRows = 1;
  Scanner patternB = new Scanner (System.in);
  
  System.out.println("Enter an integer ");//assign integer
  while (true)
  {
   if (patternB.hasNextInt() == true)
   {
    number = patternB.nextInt();
    
    if(number > 0 && number < 11)
    {
     break;
    }
   }
   System.out.println("Wrong Integer");
   System.out.println("Enteger should be 1~10");
   patternB.nextLine();
  }
  
  for (numRows = 1; numRows <= number; number--) // num decreases
  {
   for (int i = 1; i <= number; i++) // print num till it equals to user input
    {
     
     System.out.print(i + " ");
     
    }
    System.out.println("");
   
   
  }
 }
}