//HyungJun Nam

import java.util.Arrays; //Arrays import

public class lab08 {
 public static void main (String[]args)
 {
  int[] A1 = new int[100]; 
  int[] A2 = new int[100];
  for (int i = 0; i < A1.length; i++)
  {
   A1[i] = (int)(Math.random()*100); //assigns 1 ~ 100

  }

  System.out.print("A1 has these integers: " + Arrays.toString(A1).replace("[", "").replace(",", "").replace("]", "")); //Print the Array
  System.out.println("");
  for (int a = 0; a < A1.length; a++) //counting and assigning A2
  {
   for (int b = 0; b < A1.length; b++)
   {
    if (A1[a] == b)
    {
     A2[b] = A2[b] + 1; //counting
    }
   }
  }
  
  for (int c = 0; c < A1.length; c++) //print result
  {
   System.out.println(c + " occurs " + A2[c] + " times.");
  }
 }
}
